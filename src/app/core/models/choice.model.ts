import { Answer } from '../enums/answer.enum';
import { Owner } from './owner.model';

export class ChoiceGroup {
	date_string: string;
	subSetToYes = false; // to know if all the choices are set to YES, to toggle them all at once without checking them individually
	choices: Choice[];
}
export class AnswerStats {
	count: number;
	people: Array<string>;
}
export class ChoiceText {
	public name: string; // text of the choice, displayed as an option
	public url_href: string; // url used as clickable link on a picture
	public url_display: string; // url to fetch a picture to display, this picture might be clickable with url_href filled
}
export class Choice {
	public id: number;
	public name: string;
	public created_at: string;
	public score: number;
	public enabled: boolean;
	public url?: string;
	public yes?: AnswerStats;
	public no?: AnswerStats;
	public maybe?: AnswerStats;

	constructor(
		public participants: Map<Answer, Set<Owner>> = new Map<Answer, Set<Owner>>([
			[Answer.YES, new Set<Owner>()],
			[Answer.NO, new Set<Owner>()],
			[Answer.MAYBE, new Set<Owner>()],
		]),
		public counts: Map<Answer, number> = new Map<Answer, number>([
			[Answer.YES, 0],
			[Answer.NO, 0],
			[Answer.MAYBE, 0],
		])
	) {}

	public updateParticipation(user: Owner, responseType: Answer): void {
		this.removeParticipant(user);
		this.participants.get(responseType).add(user);
		this.updateCounts();
	}

	public removeParticipant(user: Owner): void {
		for (const responseType of Object.values(Answer)) {
			if (this.participants.get(responseType).has(user)) {
				this.participants.get(responseType).delete(user);
			}
		}
	}

	public updateCounts(): void {
		for (const responseType of Object.values(Answer)) {
			this.counts.set(responseType, this.participants.get(responseType).size);
		}
	}
}
