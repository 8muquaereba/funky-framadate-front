import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
	providedIn: 'root',
})
export class DateService {
	public static addDaysToDate(days: number, date: Date): Date {
		return moment(date).add(days, 'days').toDate();
	}

	public static diffInDays(dateLeft: Date, dateRight: Date): number {
		return moment(dateLeft).diff(moment(dateRight));
	}

	public static isDateInFuture(date: Date): boolean {
		return this.diffInDays(date, new Date()) > 0;
	}

	public static isDateInPast(date: Date): boolean {
		return this.diffInDays(date, new Date()) < 0;
	}

	public static formatDate(date: Date): string {
		return moment(date).format('yyyy-MM-dd');
	}
}
